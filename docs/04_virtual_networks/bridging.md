## Bridging

An alternative to NAT'ing is to bridge. The effect is that the VM is directly connected to the subnet of the host.

For this to work, you must have a cabled connection. Wiresless connection may or may not work depending of the specific wireless settings.

## Bridging

1. Describe in your own words what bridging is

    Include OSI model terminaology


## Bridging in vmware


1. Make a network diagram showing the VM connected using bridging
1. modify the network setting for your kali linux to bridging
3. Spin up kali
4. Document that you are bridging.
