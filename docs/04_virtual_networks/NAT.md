# NAT

Network address translation is the mechanism used to hide internal networks and use a routers outside IP address.

## Networks in vmware

You have different options for the network setings in vmware. See e.g. [vmware docs](https://kb.vmware.com/s/article/1018697) for a guide.

1. Read up on it in [the official docs](https://docs.vmware.com/en/VMware-Workstation-Pro/15.0/com.vmware.ws.using.doc/GUID-0CE1AE01-7E79-41BB-9EA8-4F839BE40E1A.html)
2. Write an overview of the different types of networks available for vmware VMs.


## NAT network

1. Find or redo the networks diagram where you use the Kali linux VM using NAT.

    Please add all ip adresses and vmware subnet number+name

2. Spin up Kali linux and verify your network

3. Document that you are actually NAT-ting.
