# Virtual networks

## Goals

The goal for this part is for the student to

* be able to explain the different ways virtual machines may connect to networks
* be able to set up internal networks in vmware

There are three subparts:

* [NAT](NAT.md)
* [Bridged](bridging.md)
* [Host only](internal.md)

The first is a recap about NAT-ting, see also NAT in [section 3](../03_IP/routing.md#NAT-ting). Second part is about making physical layer 2 connection outside the host, and the third part is about connecting VMs only to each other or the host.
