# HTTP protocol

## HTTP basic concepts

The internet is overflowing with relevant resources

* Official standard, see [RFC7231](https://tools.ietf.org/html/rfc7231) and associated RFCs.
* From [cloudflare](https://www.cloudflare.com/learning/ddos/glossary/hypertext-transfer-protocol-http/)
* From [tutorialspoint](https://www.tutorialspoint.com/http/http_overview.htm)
* From [mozilla](https://developer.mozilla.org/en-US/docs/Web/HTTP/Overview)
* From [Wikipedia](https://en.wikipedia.org/wiki/Hypertext_Transfer_Protocol)

Key takeaways:

* We send requests, and receive responses.
* Requests use methods: GET, PUT, POST, etc.
* There are a lot of info in the headers, both in request and reponse.
* There are return codes, e.g. 200 "OK", 302 "redirect", 404 "not found" and (way) more.

Previously, we have not touched upon TCP which most HTTP traffic uses. For our purposes, [TCP](https://en.wikipedia.org/wiki/Transmission_Control_Protocol) is the layer 4 protocol that handles the connection between client and the server, and ensure that all packages arrive and are in order.

Default for HTTP traffic is TCP port 80 and default for HTTPS is TCP port 443. The port number may be seen as the identifier for which program on the server to connect to.

## Initial browser connection (30 min)

1. Start the Kali VM
2. Start `wireshark`
3. In a browser go to `http://ftp.debian.org`

    Note: Even though it is called, "ftp" dot something, that is just the name of the server, and the `http://` determines the protocol to use.

    Note 2: This is the debian package repository for downloading program using `apt`. That communication is plain-text is a pricavy issue, not a security issue, since each package i signed and a lot of security is implemented that way.

4. Refind the packets in wireshark.

    Relevant display filters are `tcp.port == 80` or `http`.

    Or find DNS traffic to find the IP address of `ftp.debian.org` that is used, and filter on that using `ip.addr`

5. Compare what you see in the browser with what you see in wireshark.

    In wireshark, there is an option for [following streams](https://www.wireshark.org/docs/wsug_html_chunked/ChAdvFollowStreamSection.html), notably in this context "follow TCP" and "follow HTTP stream".

    Use the latter to avoid looking at compessed content.

6. Write your conclusions, ie. which methods used, return codes, the number of requests, content, and if there are anything else you notice.
