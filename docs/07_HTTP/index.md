# HTTP and HTTPS

## Goals

The goal for this part is for the student to

* explain how the HTTP protocol works
* explain the practical differences between HTTP and HTTPS
* explain the infrastructure needed to use HTTPS

There are three parts:

* [HTTP protocol](http_protocol.md)
* [web clients](web_clients.md)
* [HTTPS](https.md)

The first part is an introduction to HTTP, followed by looking into browsers and the third part is encryption.
