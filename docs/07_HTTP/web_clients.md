# Web clients

A client is a program that connects to a server. The server just waits for the client to take the initative. This is a definition.

* A web server is program, that acts a server and can talk HTTP and/or HTTPS. It will normally be reachable on port tpc/80 for HTTP and port tcp/443 for HTTPS. These are the default values, and may be different.
* A web client is program, that acts a client and can talk HTTP and/or HTTPS. There a many of these ranging from simple python programs, full-fletched specialized HTTP/S CLI programs (like `wget` and `curl`) to large GUI client (like `firefox`, `chrome`, `edge`, `safari`). This last group is normally refered to as "browsers". A personal favorite is [`w3m`](https://sourceforge.net/projects/w3m/) an old text mode browser.

## Fetching on the command line (15 min)

1. Start a terminal in Kali

2. Run `curl http://ftp.debian.org`

    Explain what is seen.

3. Run `curl -v http://ftp.debian.org`

    Explain what is seen.

7. Make a short writeup of your findings.

## Inspecting HTTP traffic (30 min)

1. Use `firefox` on Kali

    You may want to run `sudo apt-get install open-vm-tools-desktop` first, so you may benefit from a higher resolution.

2. Open the inspection window using ctrl+shift+i

2. Go to `http://ftp.debian.org`

4. In the "inspector" tab, you will be able to see the html of the page. Ensure that it looks as expected.

5. Go to the "network" tab. Notice the status codes, multiple "GET"s and the 404 for the favicon.

    * What do you "GET"?
    * What is favicon? there is a return code of 404, is that an error?

6. Reload the page.

    * Do you get the same traffic?
    * What does "cached" mean in this context? Check the "If-Modified-Since" field in the request header.

7. Make a short writeup of your findings.

This concept of inpection exists in most browsers and is very useful for debugging. It has the extra advantage of working even with HTTPS, since it show information after decryption.

The inspect tool is powerfull, but off-topic for more than the above.

## Inspect in you own browser (15 min)

1. Use your favorite browser and the inspection tool on your laptop to access `http://ftp.debian.org`,

    Some hints for [different browsers](https://www.lifewire.com/get-inspect-element-tool-for-browser-756549)

    Browser specific links, mostly for reference
    * [Mozilla firefox](https://developer.mozilla.org/en-US/docs/Tools/Page_Inspector/How_to/Open_the_Inspector),
    * [chrome dev tools](https://developers.google.com/web/tools/chrome-devtools)
    * A random video about it from [InMotion hosting](https://www.youtube.com/watch?v=WfKG9C00giI)

2. Check that you get very similar information as the previous exercise.

3. (Bonus) Browse around a bit to get a feel for the different website, and how much they download.

    Check [this](https://medium.com/positivenaick-analytics/5-crazy-hacks-using-inspect-element-6aabccec94c9) for inspiration.
