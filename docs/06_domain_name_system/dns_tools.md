# DNS tools

Since DNS is so fundamental, there are a lot of tools available. We will focus mostly on `dig`.


## dig server (20 min)

1. Run `dig google.com`

    Read the output and explain what is seen

1. Run `dig www.google.com`

    Read the output and explain what is seen

1. Run `dig drive.google.com`

    Read the output and explain what is seen

1. Run `dig meet.google.com`

    Read the output and explain what is seen

4. Explain the differences


## dig reverse (15 min)

1. Run `dig google.com`

    Read the output and verify that it is as expected.

1. Run `dig -x <ip>`, where `<ip>` is the ip address found in the step above.

    Read the output and explain what is seen

2. Explain what the two domains are not the same

3. Run `ping -c1 google.com`

    Read the output, explain what is seen, and verify that it is as expected.

## dig +trace (15 in)

The tool `dig` has an option for tracing the DNS requests needed to look up a name.

1. Run `dig www.gitlab.com`

    Read the output and explain what is seen

2. Run `dig +trace www.gitlab.com`

    Read the output and explain what is seen

    `NEC3` and `RRSIG` are security related and may be ignored for now.


## dig records (20 min)

1. Run `dig google.com`

    Read the output and verify that it is as expected.

1. Run `dig google.com A`

    Read the output and verify that it is as expected.

1. Run `dig google.com MX`

    Read the output and explain what is seen.

1. Run `dig google.com NS`

    Read the output and explain what is seen.

## dns and wireshark (20 min)

1. Run wireshark and start capture on the uplink interface

    The uplink interface is the one that has the connection to the internet.

2. Check the [wireshark filter reference](https://www.wireshark.org/docs/dfref/) for how to filter DNS queries

3. In a terminal, run `dig google.com`

4. Stop the capture and refind the packages

5. Verify that the packages show the same as the output in the terminal.
