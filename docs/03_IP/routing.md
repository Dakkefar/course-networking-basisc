# Routing

On layer 2, all devices talk directly wth each other using physical addressing. Layer 3 is using logical addresses to enable device on different
subnets to communicate. The logical addresses normally used is IP.

In order for a device to communicate across subnets, it has to go through a device on its own subnet. Devices that forwards data from one subnet to another based on ip adresses are called a router.

[A video](https://www.youtube.com/watch?v=JOomC1wFrbU) describing the above by udacity.

## Routing in Kali VM

1. Spin up Kali and start a terminal
2. Use `ip route` to see all known routes
3. Explain what is seen

    How many interfaces are configured?

4. Use `ip route get 8.8.8.8` and `ip route get <local ip>` where `<local ip>` is an ip addres on your local subnet.
5. Explain what is seen
6. Put result in shared document

## Routing on host

1. Spin up Kali
2. On your host use `ip route` (for linux) and ` route print -4` to see all known ipv4 routes

    Info on windows route command may be found [here](https://sourcedaddy.com/networking/using-route-command.html).

3. Explain what is seen

    How many interfaces are configured?

4. On Linux, use `ip route get 8.8.8.8` and `ip route get <local ip>` where `<local ip>` is an ip addres on your local subnet. On windows, you need to use powershell, or look in the routing table manually.

    Info on Find-NetRoute from [microsoft docs](https://docs.microsoft.com/en-us/powershell/module/nettcpip/find-netroute?view=win10-ps).

5. Explain what is seen
6. Put result in shared document

## NAT-ting

A common setup is to use private ip addresses in-house on the local networks, and then use NAT when communicating with the internet.

[Video about NAT](https://www.youtube.com/watch?v=FTUV0t6JaDA) from powercert

NAT is an acronym for Network Address Translation, and, from a practical pointof-view, it has the effect of hiding the inside network, and makes it seems as if all traffic from the inside network is originating at the router.

## Wireshark and NAT

1. spin up Kali VM
2. In Kali, start wireshark and enable packet capture
3. On the host, start wireshark and listen on the outside interface.

    The outside interface is the interface that connect to the outside world as oposed to an internal interface which the Kali VM is connected to.

2. In Kali, `ping 8.8.8.8`
3. Refind the ping traffic on both Kali and the host.
4. Compare and explain the differences on all layers.
5. Put you results in a shared documents.
