# Bridging

The network we will using is shown below

![router-two-hosts network](router_two_hosts.png)

The main idea is to have a shared network between two physical hosts, while maintaining separate internet access. A physycal ethernet (RJ45) port is required.

## Designing (30 min)

1. Think about this...

    * What equipment is needed, ie. what do we have on layer 1?
    * How should packets be routed from kali to kali? What is required knowledge of the different devices?
    * How can we send packets both to the internet and to the other host?
    * Why is it important that the/some DHCP server is disabled?
    * Are there any restraints to choosing ip addresses for subnets? (spoiler: yes!)

1. In pairs, redo the diagram and add your own IP adresses.

    Remember to include the names of the interfaces and vmnet numbers.

3. Get approval :-)

4. Upload to shared doc.


## Implementing (90 min)

This is an advanced exercise. When you get stuck, lean back and ask yourself

1. what you are trying to achive
2. what do I think is wrong
3. how to substantiate it
4. How to fix

Obviously, google will be helpful for certain things - but that requires the correct search terms. Those might appear at questions 1-3, if not ask someone to regain the big picture.

It is important that you takes notes so you are able to recreate this. Document each step, so you yourself understand what yo udid when you look at it 2 months from now.

1. Download the no-dhcp router from [gdrive](https://drive.google.com/file/d/1wT5Htqx5c7ob5TFy8PsRJGmUrIz6JtS6/view?usp=sharing)

    If you know OpenBSD, feel free to reimport the other and tweaks to match your plan.

2. Set up the vmnets as you decided, import the router and connect networks correctly.

2. Update the ip addresses inside the router

    There is a quick guide [here](../Bonus/ob_ip_update.md)

3. In the router use `ifconfig` to verify that ip addresses are as intended.

4. Use `netstat -nrf inet` to see the routing table

    The specific command isn't important, except to know that it shows the routing table for ipv4. For more info about netstat go [here](https://man.openbsd.org/netstat)

5. Explain what is seen.

6. Set up kali linux with the ip address you decided during design, and the default gateway

    See e.g. the [previous exercise about that](../03_IP/ip_addresses.md).

7. Test that you are able to ping the router and the internet.

    If this or the subsequent tests fails, look at the diagram and decide where to sniff traffic to get an idea of where and waht is wrong.

    The router has `tcpdump` installed, which is kind of a text-mode wireshark. Minimum survival command is `tcpdump -n -i em0` to see the traffic on the uplink interface. Go  [here](https://wizardzines.com/zines/tcpdump/) for a primer.

8. Test that you can ping the internet and kali from the router.

9. Check that you can ping the other router.

8. To get access to the subnet on the other host, you must create a static route on the router.

    See examples [here](https://www.cyberciti.biz/faq/howto-setup-static-routes-on-openbsd-unix-networking/)

9. Put the process and the results in a shared doc.
