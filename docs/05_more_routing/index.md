# More routing

## Goals

The goal for this part is for the student to

* be able to use virtual machines with multiple subnets
* be able to debug and document routing in a virtual and physical network

There are two subparts:

* [VMware setup and imports](setup.md)
* [Bridging](bridging.md)

The first part is about how to import an .ova file and use it, and the second part is about connecting two physical machines and expose their internal networks.
