# Updating ip adresses in OpenBSD

For our purposes, the relevant files are called `/etc/hostname.em1` and `/etc/hostname.em2`.

The official doc are [here](https://man.openbsd.org/hostname.if.5) and [the faq](https://www.openbsd.org/faq/faq6.html)

Note that the default keyboard in the terminal is US.

1. log in to the router

    Username: `root`, password: `root123`

2. Install `nano` (a text editor), using `pkg_add nano`

2. Use `nano /etc/hostname.em1` and update ip address (and perhaps) netmask.

    This is the second interface, where the first `em0` is the upload and uses dhcp.

    Ignore the comments on the first line.

2. Use `nano /etc/hostname.em2` and update ip address (and perhaps) netmask.

    This is the third interface

3. Apply the changes, using `sh /etc/netstart`

4. Test that it works by pinging internal and external addresses.
