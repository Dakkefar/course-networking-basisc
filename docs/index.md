# Networking

The aim for this site is to be a collection of resources for students that want to learn basic networking.

The goal is for the student to have operational understanding and skills in relation to networking. Theory will be minimal, and hands-on is emphasized.

**This is still WIP**

## Introduction

Getting to know the basic concepts and tools.

Go [here](01_Intro/index.md)

## OSI model and packets

Going through the OSI model, and use Wireshark to see the data on the wire.

Go [here](02_OSI/index.md)

## Layer 3: IP, routers and routing

IP adresses are used for routing, and to get beyond the local subnet.

Go [here](03_IP/index.md)

## Virtualized networks

Using VMware workstation or similar virtualization software gives options as to how a virtual machine is connected to both physical and virtual networks.

Go [here](04_virtual_networks/index.md)


## More routing

Routing using virtual machines in vmware workstation

Concepts: IP, virtual machines, network layout

Skills: Use `VMware workstation` (Importing VMs, creating vmnets, connecting VMs to vmnets,), use `wireshark`, draw diagrams

Go [here](05_more_routing)

## Domain name system

DNS is the mechanism that translates from domain names to IP adresses.

Concepts: IP, domain names, DNS protocol (RFC1034 and RFC1035), DNS record, DNS server infrastructure

Skills: Use `dig`, use `wireshark`, simple Linux CLI

Go [here](06_domain_name_system)

## HTTP and HTTPS

HTTP (and HTTPS) is the protocol users see the most. It is the one used to "surf" the web. It is increasingly used for backend systems also.

Concepts: IP/domain names, HTTP protocol ([RFC7231](https://tools.ietf.org/html/rfc7231)), HTTPS, web server and clients, symmetric and asymmetric encryption, certificates, TLS, AES

Skills: Use `wget`, use `wireshark`, simple Linux CLI

Go [here](07_HTTP)

## Layer 5+: Application layer protocols

We will look at HTTP/HTTPS in detail as an example of an application layer protocol

(Material to come)


## Layer 2: Subnets, switches and VLANs

Layer 2 includes some network mechanics that will be explored.

(Material to come)


## Network devices

There exists a wide range of network devices, that may/may not be relevant when designing networks. This relates strongly to the OSI model.

Modem, switches, routers, firewalls, Next-gen firewalls, Access points, WLC.

(Material to come)

## Designing networks

Proper usage of VLANs, subnets, IP layout will be addressed. Topologies also.

(Material to come)

## Monitoring

How to monitor that the network and the related services is behaving as intended.

(Material to come)
