# Introduction

## Goals

The goal for this part is for the student to

* understand the structure and overall goals of the course
* have a superficial structural understanding of networks
* have the tools needed for the exercises in the course

There are two subparts:

* [Networking basics](Network_basics.md)
* [Virtualization](Virtualization.md)

The first part is to get you acquainted with networking concepts. The second is to have the tools necessary to do the hands-on exercises.
